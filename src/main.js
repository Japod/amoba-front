import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";
import VueAxios from "vue-axios";
import store from "./store/store";
import Notify from 'vue2-notify'

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";

// Import Bootstrap an BootstrapVue CSS files (order is important)
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import "./assets/styles/index.css";

Vue.config.productionTip = false;

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue);
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin);
// Axios Declarations
Vue.use(VueAxios, axios);
// Notifications
Vue.use(Notify)

Vue.axios.defaults.baseURL = "http://127.0.0.1:8000/api";

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");

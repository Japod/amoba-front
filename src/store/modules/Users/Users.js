import Vue from "vue";

const state = {
  users: [],
  selectedUser: {},
  modal: false,
};

const getters = {
  users: ({ users }) => users,
};

// Mutations
const mutations = {
  SET_USERS(state, payload) {
    state.users = payload;
  },
  CREATE_USER(state, user) {
    state.users.push(user);
  },
  UPDATE_USERS(state, user) {
    const UsersIndex = state.users.findIndex((p) => p.id === user.id);
    Object.assign(state.users[UsersIndex], user);
  },
  REMOVE_USER(state, itemId) {
    const ItemIndex = state.users.findIndex((p) => p.id === itemId);
    state.users.splice(ItemIndex, 1);
  },
  OPEN_MODAL(state) {
    state.open = true;
  },
  CLOSE_MODAL(state) {
    state.open = false;
  },
};

const actions = {
  GET_USERS({ commit }) {
    return Vue.axios
      .get("users")
      .then((data) => {
        commit("SET_USERS", data.data.data);
      })
      .catch(() => {});
  },
  DELETE_USER(context, payload) {
    console.log("Payload", payload);
    return Vue.axios
      .delete("users/" + payload)
      .then(() => {
        context.commit("REMOVE_USER", payload);
      })
      .catch(() => {});
  },
  PUSH_USER({ commit, payload }) {
    commit("CREATE_USER", payload);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};

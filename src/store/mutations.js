const mutations = {
  CHANGE_MODAL(state) {
    state.openModal = !state.openModal;
  },
};

export default mutations;

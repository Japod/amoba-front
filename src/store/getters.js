const getters = {
  openModal: ({ openModal }) => openModal,
};

export default getters;
